"use strict";

const { ApolloServer } = require('apollo-server');
const { ApolloGateway } = require('@apollo/gateway')
const port = 4000;

const gateway = new ApolloGateway({
    serviceList: [
        { "name": "lending", "url": "https://secure-dev.ally.com/acs/cloud-lab-czl74b-lending-api-105082-default" },
        { "name": "party", "url": "https://secure-dev.ally.com/acs/czl74b-party-czl74b-party" }
    ]
});
const server = new ApolloServer({
    gateway,
    introspection: true,
    playground: true,
    subscriptions: false
    // context: ({ event, context }) => ({
    //     headers: event.headers,
    //     functionName: context.functionName,
    //     event,
    //     context,
    // }),
});

server.listen({ port }).then(({ url }) => {
    console.log(`Gateway Server ready at ${url}`)
});

// exports.graphqlHandler = server.createHandler();

// exports.graphqlHandler = server.createHandler({
//     cors: {
//         origin: '*',
//         // credentials: true,
//         methods: 'GET, POST',
//         allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
//     },
// });
