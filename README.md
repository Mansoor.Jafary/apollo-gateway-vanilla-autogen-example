# Description
A vanilla implementation of the Apollo Gateway Server. This server auto generates a super graph based on the subgraphs provided.

Note that the sub-graphs must follow the Apollo Federation Spec to be compliant with this example.

## How to Run  

```npm install```  
``` npm start ```

## Sample Queries  

```
{
  productAccountByAccountNumber(input: {proxyAccountNumber: "X001"}) {
    proxyAccountNumber
    ... on LineOfCreditAccount {
      financialPosition {
        balancesPosition {
          metrics
          currentBalance {
            amount
            currency
          }
        }
      }
    }
  }
}
```

```
{
  partyByAccountNumber(input: {proxyAccountNumber: "X001"}) {
    accounts {
      proxyAccountNumber
      ... on LineOfCreditAccount {
        financialPosition {
          balancesPosition {
            metrics
            currentBalance {
              amount
              currency
            }
          }
        }
      }
    }
  }
}
```
